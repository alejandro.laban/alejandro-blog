---
title: Cloudflare Pages and Gitlab Configuration
date: 2023-02-27T04:19:46.716Z
author: Alejandro Laban
tags:
  - blog
  - Hugo
  - Gitlab
  - CI/CD
  - Cloudflare
description: Continuing with this blog series, we will look into Cloudflare
  configuration and Gitlab CI/CD configuration.
draft: false
showToc: true
TocOpen: true
hidemeta: false
comments: false
disableShare: false
disableHLJS: false
hideSummary: false
searchHidden: false
ShowReadingTime: true
ShowBreadCrumbs: true
ShowPostNavLinks: true
ShowWordCount: true
ShowRssButtonInSectionTermList: true
UseHugoToc: true
cover:
  image: <image path/url>
  alt: <alt text>
  caption: <text>
  relative: false
  hidden: true
editPost:
  Text: Suggest Changes
  appendFilePath: true
---
## Cloudflare Pages Configuration

A pre-requisite is to have a working domain on Cloudflare, if you don't have one, we can buy one and then point it to Cloudflare DNS.

After we enable our site on Cloudflare, we will navigate to `Pages`, click on `Create a project` select `Direct Upload`, fill in `Name` and select your `public` folder on your repository. If it is empty, just build it with `hugo`.

After uploading is complete, our blog will be visible on its domain; however, you may be wondering, "What if I want to add a new entry or change something?" and Do I need to build it again and upload it by hand? Well, yes, that's how a static site behaves; however, we could use a pipeline tool to do it on new updates without our intervention.


## Gitlab CI/CD Configuration

To start with our pipeline configuration, we need two values from Cloudflare to connect it to our Gitlab account; account id and an API token. We will be using two jobs on our pipeline, one for Hugo build and another to deploy it using the Cloudflare wrangler CLI


### Getting our variables

First, we will enter our Cloudflare home, and from there, we will get our account ID from our url, which will have this format: https://dash.cloudflare.com/<account-id>.

Next, we will need an API token from Cloudflare to upload to our Cloudflare pages. To do so, go to Cloudflare's `My Profile` configuration, then to `API Tokens` then to `Create a token` and finally to `Create a custom token`. Next, we will enter its `Name`, and on `Permissions`, we will select our three drop-downs, `Account`, `Cloudflare Pages`, and `Edit`, before reviewing and saving it and copying our token.

### Setting our variables

We will open Gitlab CI/CD and navigate to our repository, then go to "Settings" -> "CI/CD" -> "Variables" and add CLOUDFLARE_ACCOUNT_ID and CLOUDFLARE_API_TOKEN. 

```yaml
CLOUDFLARE_ACCOUNT_ID: ID
CLOUDFLARE_API_TOKEN: TOKEN
```

### Pipeline configuration

Now that we have our variables ready, we will concentrate on the pipeline definition, which will only require two jobs in this case: one to build our assets and another to deploy them.

We will create our gitlab-ci.yml with the following content:

```yaml
variables:
  GIT_SUBMODULE_STRATEGY: recursive

build:
  stage: build
  image: klakegg/hugo:alpine-ci
  only:
    - main
  script:
    - hugo
  artifacts:
    paths:
      - public

deploy:
  stage: deploy
  image: node:latest
  environment:
    name: production
    url: https://alejandrolaban.com/
  only:
    - main
  script:
    - npx wrangler pages publish public --project-name ${CI_PROJECT_NAME} --branch ${CI_DEFAULT_BRANCH}
```
Something to point out is variable `GIT_SUBMODULE_STRATEGY` as our theme resides in its own repository and needs to be fetched. The building result will appear in the public folder that we set as an artifact to be downloaded on the next deploy job, where it will already have been downloaded and copied, and then using wrangler we will upload it to Cloudflare pages.

Now that we have our pipeline in place, we can update or create one blog entry and publish it, which will trigger a pipeline job `build` and then a `deploy`. After they finish, we will see our live blog updated.

