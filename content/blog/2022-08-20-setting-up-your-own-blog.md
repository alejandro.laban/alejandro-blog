---
title: Setting up your own blog
date: 2022-08-20T17:17:30.386Z
author: Alejandro Laban
tags:
  - blog
  - PaperMod
  - Hugo
  - GitLab
description: Following this blog series, we will review how to create a new
  site, add a theme, and preview it.
draft: false
showToc: true
TocOpen: true
hidemeta: false
comments: false
disableShare: false
disableHLJS: false
hideSummary: false
searchHidden: false
ShowReadingTime: true
ShowBreadCrumbs: true
ShowPostNavLinks: true
ShowWordCount: true
ShowRssButtonInSectionTermList: true
UseHugoToc: true
cover:
  image: <image path/url>
  alt: <alt text>
  caption: <text>
  relative: false
  hidden: true
editPost:
  Text: Suggest Changes
  appendFilePath: true
---
To create your blog, you need to follow some steps:

- Install Hugo
- Create a new sit
- Add a theme
- Sync a push to a remote git repository

## Installing Hugo

There are some alternatives to installing Hugo. Let's use Homebrew (Linux and MacOS).

```shell
brew install hugo
```

## Creating a new site

Run the following command `hugo new site <site-name> -f yaml`

```shell
hugo new site alejandro-blog -f yaml
```

That command will create a new site in the current folder with the name alejandro-blog and it will output something similar to this:

```bash
Congratulations! Your new Hugo site is created in /home/alejandro/Documents/Personal/alejandro-blog.

Just a few more steps and you're ready to go:

1. Download a theme into the same-named folder.
   Choose a theme from https://themes.gohugo.io/ or
   create your own with the "hugo new theme <THEMENAME>" command.
2. Perhaps you want to add some content. You can add single files
   with "hugo new <SECTIONNAME>/<FILENAME>.<FORMAT>".
3. Start the built-in live server via "hugo server".

Visit https://gohugo.io/ for quickstart guide and full documentation.
```

## Sync a push to a remote git repository

Now that we have our brand new blog, we can sync it with a git repository and push it to the remote.

First you need to create a remote git repository (e.g. GitLab, GitHub, Bitbucket), then you need to initialize your folder where your site resides,commit your changes, add the remote, and push it.

For those steps, we will have something similar:

```bash
git init
git branch -m main
git add .
git commit -m "brand new blog"
git remote add origin https://gitlab.com/alejandro.laban/alejandro-blog.git
git push -uf origin main
```

## Adding a theme

Here you can choose any template. Each theme has its own unique features and configuration, but the installation process will be similar.

Most themes will be available on Github, and you will only need to add them as sub-modules inside the themes folder to install them. 

Here we are going to add a theme called PaperMod from Github on `https://github.com/adityatelange/hugo-PaperMod`

For that, we need to add it as a git sub-module and initialize that sub-module.

```shell
git submodule add --depth=1 https://github.com/adityatelange/hugo-PaperMod.git themes/PaperMod
git submodule update --init --recursive
```
You should now see a new folder themes/PaperMod as a result of those commands. 

Now we need to reference it in the Hugo site configuration config.yml.

For that, just add `theme: "PaperMod"` in config.yml:

```yaml
theme: "PaperMod"
```
If you run a Hugo server, you should now see your site with your theme running on your localhost on port 1313, which you can access with your browser at http://0.0.0.0:1313. 

The next post will be about configuring your site, theme, and writing your first post.

references:

- https://gohugo.io/getting-started/installing/
- https://gohugo.io/getting-started/quick-start/
- https://github.com/adityatelange/hugo-PaperMod