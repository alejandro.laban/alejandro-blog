---
title: My blogging journey
date: 2022-07-28T05:39:06.208Z
author: Alejandro Laban
aliases: []
tags:
  - blog
  - Hugo
  - Gitlab
  - Cloudflare
  - Netlify CMS
  - PaperMod
description: This is a walkthrough of my journey to create my blog, something
  that I thought about quite some time ago and now is the time to give it a try
  and document this process in this blog series.
draft: false
showToc: true
TocOpen: true
hidemeta: false
comments: false
disableShare: false
disableHLJS: false
hideSummary: false
searchHidden: false
ShowReadingTime: true
ShowBreadCrumbs: true
ShowPostNavLinks: true
ShowWordCount: true
ShowRssButtonInSectionTermList: true
UseHugoToc: true
cover:
  image: <image path/url>
  alt: <alt text>
  caption: <text>
  relative: false
  hidden: true
editPost:
  Text: Suggest Changes
  appendFilePath: true
---
## Where do I start?

Well, you need somewhere to host your publications. There are plenty of alternatives from low to high complexity. To name a few nowadays, we use:

- Wordpress (with deployment as SaaS, hosting-services, self-hosted)
- Git backed
   - Gitlab pages
   - Github pages
- Static generators
   - Hugo
   - Gatsby
   - Jekyll
   - Nuxt
- Medium
- Ghost

Each one having its own set of advantages and drawbacks on blog creation, daily use, deployment and maintenance

## What alternatives should I choose?﻿

That depends on your background. Most people will use Wordpress due to it being easy to use and almost every hosting provider will provide it. However, your data will be mixed between files and a database.

I would recommend choosing Hugo as it enables you to do it at a low cost and with a high degree of freedom regarding where you can deploy it and migrate it to another place if needed with low effort.

## Selected Technologies

The stack of technologies to use can vary and that can depend on the person who implements it. From my personal experience, I will choose the following:

- Static content generator (generates static sites from code)
   - Hugo
      - theme: PaperMod
- CMS (Content management system—useful for creating content using a UI)
   - Netlify CMS
      - backend: gitlab
- Repository (where my content will reside)
   - Gitlab
- DNS service (to route my custom domain to my site)
   - Cloudflare DNS
- CDN (Content delivery Network - it will serve my static site blog)   
   - Cloudflare Pages

The next post will be about how to setup a Hugo site, theme and Netlify CMS.