---
title: Theme configuration and CMS setup
date: 2023-02-25T22:44:45.169Z
author: Alejandro Laban
tags:
  - bloging
  - Hugo
  - Gitlab
  - Cloudflare
  - Netlify CMS
  - PaperMod
description: Continuing with our blog series about creating our blog from
  scratch, in this episode we will finish our theme configuration and add our
  CMS.
draft: false
showToc: true
TocOpen: true
hidemeta: false
comments: false
disableShare: false
disableHLJS: false
hideSummary: false
searchHidden: false
ShowReadingTime: true
ShowBreadCrumbs: true
ShowPostNavLinks: true
ShowWordCount: true
ShowRssButtonInSectionTermList: true
UseHugoToc: true
cover:
  image: <image path/url>
  alt: <alt text>
  caption: <text>
  relative: false
  hidden: true
editPost:
  Text: Suggest Changes
  appendFilePath: true
---
## Theme configuration

Now that we have our initial blog ready, we need to configure it.

Most configuration parameters belong to our chosen theme, there is an example config.yml file in the paperMod theme repository:

https://github.com/adityatelange/hugo-PaperMod/blob/be00e5a557212de1067016f0fe8a4aa00d23f2e2/config.yml

Some important values to update are:

* baseURL
* params.title
* params.description
* params.keywords
* params.editPost.URL
* menu.main
* googleAnalytics

Others are optional and depend on the features of your theme that you want to use. After you update those values, we can review them by running `hugo server`, With this, we can see our initial page with its header, main content, and footer rendered properly with our config.yml values.

## CMS set up

Next, we will set up Netlify CMS to simplify our blog page creation. We can skip this step, but every time we would like to add an entry to our blog, we will do it directly on text, commit it to git, and push it to its remote.

Fortunately, it is easy to setup Netlify CMS we will do the following:

* In our static/admin folder, create an index.html file.
* Place a config.yml file in our static/admin directory.

index.html will have the following content:

```html
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Content Manager</title>
    <!-- Include the script that enables Netlify Identity on this page. -->
    <script src="https://identity.netlify.com/v1/netlify-identity-widget.js"></script>
</head>
<body>
<!-- Include the script that builds the page and powers Netlify CMS -->
<script src="https://unpkg.com/netlify-cms@2.10.192/dist/netlify-cms.js"></script>
</body>
</html>
```

config.yml will be more complex as it will have configuration for a git repository, publishing mode, and our new blog entry page with its options.

Let's break it down:

### Backend configuration

In this case, we will be using gitlab as the backend with an OAuth PKCE that will merge our changes into our main branch.

To obtain our app_id we will set up gitlab configuration in User Settings (profile) -> Applications. Fill in `Name` field and `Redirect URI` with our domain (e.g., https://alejandrolaban.com/admin/), deselect `Confidential` checkbox, select the API scope, and click on `Save application`

```yaml
backend:
  name: gitlab
  repo: alejandro.laban/alejandro-blog
  auth_type: pkce # Required for pkce
  app_id: id
  branch: main # Branch to update (optional; defaults to master)
  squash_merges: true
```

For publish mode `editorial_workflow` it's recommended as it will allow you to create drafts, review them, and then publish them.

```yaml
publish_mode: editorial_workflow
media_folder: "static/images/uploads"
public_folder: "/images/uploads"
```

### Editor configuration

Here is our editor configuration that will simplify our publishing workflow.
This section determines where our entries and fields will be created; those fields will be translated to our meta tags in Markdown, which Hugo will then convert into our blog pages.

```yaml
collections:
  - name: 'blog'
    label: 'Blog'
    folder: 'content/blog'
    create: true
    slug: '{{year}}-{{month}}-{{day}}-{{slug}}'
    editor:
      preview: true
    fields:
      - { label: 'Title', name: 'title', widget: 'string' }
      - { label: 'Publish Date', name: 'date', widget: 'datetime' }
      - { label: 'Author', name: 'author', widget: 'string', default: 'Alejandro Laban' }
      - { label: 'aliases', name: 'aliases', widget: 'list', collapsed: true , field: { label: alias, name: alias, widget: string } }
      - { label: 'tags', name: 'tags', widget: 'list', collapsed: true , field: { label: tag, name: tag, widget: string } }
      - { label: 'Description', name: 'description', widget: 'string' }
      - { label: 'Draft', name: 'draft', widget: 'boolean', default: false }
      - { label: 'showToc', name: 'showToc', widget: 'hidden', default: true }
      - { label: 'TocOpen', name: 'TocOpen', widget: 'boolean', default: false }
      - { label: 'hidemeta', name: 'hidemeta', widget: 'hidden', default: false }
      - { label: 'comments', name: 'comments', widget: 'hidden', default: false }
      - { label: 'disableShare', name: 'disableShare', widget: 'hidden', default: false }
      - { label: 'disableHLJS', name: 'disableHLJS', widget: 'hidden', default: false }
      - { label: 'hideSummary', name: 'hideSummary', widget: 'hidden', default: false }
      - { label: 'searchHidden', name: 'searchHidden', widget: 'hidden', default: false }
      - { label: 'ShowReadingTime', name: 'ShowReadingTime', widget: 'hidden', default: true }
      - { label: 'ShowBreadCrumbs', name: 'ShowBreadCrumbs', widget: 'hidden', default: true }
      - { label: 'ShowPostNavLinks', name: 'ShowPostNavLinks', widget: 'hidden', default: true }
      - { label: 'ShowWordCount', name: 'ShowWordCount', widget: 'hidden', default: true }
      - { label: 'ShowRssButtonInSectionTermList', name: 'ShowRssButtonInSectionTermList', widget: 'hidden', default: true }
      - { label: 'UseHugoToc', name: 'UseHugoToc', widget: 'hidden', default: true }
      - label: 'cover'
        name: 'cover'
        widget: 'object'
        collapsed: true
        fields:
          - { label: 'image', name: 'image', widget: 'image', default: "<image path/url>" }
          - { label: 'alt', name: 'alt', widget: 'string', default: '<alt text>' }
          - { label: 'caption', name: 'caption', widget: 'string', default: '<text>' }
          - { label: 'relative', name: 'relative', widget: 'boolean', default: false }
          - { label: 'hidden', name: 'hidden', widget: 'hidden', default: true }
      - label: 'editPost'
        name: 'editPost'
        widget: 'object'
        collapsed: true
        fields:
          - { label: 'Text', name: 'Text', widget: 'string', default: "Suggest Changes" }
          - { label: 'appendFilePath', name: 'appendFilePath', widget: 'boolean', default: true }
      - { label: 'Body', name: 'body', widget: 'markdown' }
```

After that, we can run `hugo server` and enter https://0.0.0.0:1313/admin You will be presented with a login page that will redirect you to gitlab to log in, and then it will send you to your admin console after a successful login.

Okay, now we can create a new blog using our CMS. Click on "New blog" and start editing your first entry on your brand new CMS.

The next post will be about configuring Cloudflare Pages and our GitLab CI/CD pipelines to fully automate our blog publishing.